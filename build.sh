#! /bin/bash

here="$(realpath "$(dirname "${0}")")"
font="$(cat "${here}/_font.txt")"
avail="${here}/root/etc/fonts/conf.avail"
daemon="${here}/root/etc/fonts/conf.d"


mainFunction () {
	mkdir --parents "${avail}" "${daemon}"
	generateConf
	ln -s "/etc/fonts/conf.avail/75-emoji.conf" "${daemon}/75-emoji.conf"
}


cleanUp () {
	if [ -d "root" ]; then
		rm --recursive "root"
	fi

	if [ -f "${avail}/75-emoji.conf" ]; then
		rm --recursive "${avail}/75-emoji.conf"
	fi
}


generateConf () {
	cat assets/template.conf |
	sed "s/FONT/${font}/g" > "${avail}/75-emoji.conf"
}


prepareEnvironment () {
	set -e
	cd "${here}"
	trap "" INT QUIT TERM EXIT
	cleanUp
}


prepareEnvironment
mainFunction
